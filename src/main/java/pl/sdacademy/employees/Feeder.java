package pl.sdacademy.employees;

import pl.sdacademy.animals.bear.Bear;
import pl.sdacademy.animals.bear.BlackBear;
import pl.sdacademy.animals.bird.Bird;
import pl.sdacademy.animals.bird.Goose;
import pl.sdacademy.exception.TooFastException;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Feeder {

    List<Bear> hungryBears = new ArrayList<>();

    void feedBearWithBird(Bear bear, Bird bird) {
        try {
            bear.eat(bird);
        } catch (TooFastException e) {
            hungryBears.add(bear);
        }
    }

    public static void main(String[] args) {
        try (BufferedReader br = new BufferedReader(new FileReader("bird"))) {
            int birdSpeed = br.read(); //Can throw IOException
            Bird bird = new Goose(birdSpeed);

            new Feeder().feedBearWithBird(new BlackBear(4), bird);
        } catch (FileNotFoundException e) {
            System.err.println("File not found!");
        } catch (IOException e) {
            System.err.println("I/O error!");
        }
    }
}
