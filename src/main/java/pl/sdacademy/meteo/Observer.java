package pl.sdacademy.meteo;

public interface Observer {

    void update();
}
