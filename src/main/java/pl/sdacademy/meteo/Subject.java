package pl.sdacademy.meteo;

public interface Subject {

    void notifyAllObservers();
    void register(Observer observer);
    void deRegister(Observer observer);
}
