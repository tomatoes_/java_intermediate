package pl.sdacademy.animals.bear;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import pl.sdacademy.animals.Animal;
import pl.sdacademy.animals.bird.Bird;
import pl.sdacademy.clock.Clock;
import pl.sdacademy.clock.DateTimeClock;
import pl.sdacademy.database.Database;
import pl.sdacademy.database.SQLDatabase;
import pl.sdacademy.exception.TooFastException;


public abstract class Bear implements Animal {

    protected int weight;
    private DateTime lastMealDate;
    private Clock clock;
    private Database database;

    public Bear(int weight) {
        this.weight = weight;
        this.clock = new DateTimeClock();
        this.lastMealDate = clock.getCurrentTime();
        this.database = new SQLDatabase();
    }

    public Bear(int weight, Clock clock) {
        this(weight);
        this.clock = clock;
    }

    public Bear(int weight, Database database) {
        this(weight);
        this.database = database;
    }

    @Override
    public boolean isAlive() {
        return new Duration(lastMealDate, clock.getCurrentTime())
                .isShorterThan(Duration.standardDays(10));
    }

    public void eat(Bird bird) throws TooFastException {
        checkSpeed(bird);
        lastMealDate = clock.getCurrentTime();
        database.save(lastMealDate);
    }

    private void checkSpeed(Bird bird) throws TooFastException {
        if (bird.fly() > 10) {
            throw new TooFastException();
        }
    }

    @Override
    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Bear) {
            Bear thatBear = (Bear) obj;
            return this.weight == thatBear.weight;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return weight * 11;
    }
}




