package pl.sdacademy.animals.bird;

import pl.sdacademy.animals.Animal;

public abstract class Bird implements Animal {

    private int speed;

    public Bird(int speed) {
        this.speed = speed;
    }

    @Override
    public boolean isAlive() {
        return false;
    }

    @Override
    public int getWeight() {
        return 0;
    }

    public int fly() {
        return speed;
    }

    public abstract int sing();
}
