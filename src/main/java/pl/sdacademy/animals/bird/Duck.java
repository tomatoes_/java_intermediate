package pl.sdacademy.animals.bird;

public class Duck extends Bird {
    public Duck(int speed) {
        super(speed);
    }

    @Override
    public int sing() {
        return 0;
    }
}
