package pl.sdacademy.animals.bird;

public class Goose extends Bird {

    public Goose(int speed) {
        super(speed);
    }

    @Override
    public int sing() {
        return 10;
    }
}
